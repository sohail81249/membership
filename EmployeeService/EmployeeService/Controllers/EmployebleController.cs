﻿using Microsoft.AspNetCore.Cors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EmployeeService.Controllers
{
    [EnableCorsAttribute("*")]
    [RequHttps]
    public class EmployebleController : ApiController
    {
        public HttpResponseMessage Put([FromBody] int id, [FromBody] Employee a)
        {
            try
            {
                using (EmployeeDBEntities entites = new EmployeeDBEntities())
                {


                    var entry = (from e in entites.Employees
                                 where e.ID == id

                                 select e).FirstOrDefault();


                    //var entry = entites.Employees.FirstOrDefault(e => e.ID == id);

                    if (entry != null)
                    {
                        entry.FirstName = a.FirstName;
                        entry.LastName = a.LastName;
                        entry.Gender = a.Gender;
                        entry.Salary = a.Salary;
                        entites.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "employe id" + id.ToString());
                    }
                }

            }
            catch (Exception b)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, b);
            }
        
        }
      
        public HttpResponseMessage Get(string gender="All")
        {
            using (EmployeeDBEntities entities= new EmployeeDBEntities())
            {
                switch(gender.ToLower())
                {
                    case "all":
                        return Request.CreateResponse(HttpStatusCode.OK, entities.Employees.ToList());
                    case "male":
                        var a = ( from e in entities.Employees
                                 where e.Gender.ToLower()=="male"
                                 select e).ToList();
                           
                        return Request.CreateResponse(HttpStatusCode.OK, a);
                    case "female":
                        
                        var b= (from e in entities.Employees
                                 where e.Gender.ToLower() == "female"
                                 select e).ToList();
                        //return Request.CreateResponse(HttpStatusCode.OK, b);
                        if (b == null)
                        {
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "value for gender must be male or femaLE" + gender);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, b);
                        }
                    default:
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "value for gender must be male or femaLE"+gender);



                }


            }
        }
        public IHttpActionResult  Delete(int id)
        {
            try

            {
                using (EmployeeDBEntities entities = new EmployeeDBEntities())
                {
                    var entity = entities.Employees.FirstOrDefault(e => e.ID == id);
                    if (entity != null)
                    {
                        entities.Employees.Remove(entity);
                        entities.SaveChanges();
                        return Ok();
                    }
                    else
                    {

                         return NotFound();

                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        

        }
        [HttpGet]
        public HttpResponseMessage LoadEmployId(int id)
        {
            using(EmployeeDBEntities entities= new EmployeeDBEntities())
            {
                var entity = entities.Employees.FirstOrDefault(e =>e.ID== e.ID);
                if(entity==null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,"Employee with id="+id.ToString() + "notFound");
                }
            }
        }
        public HttpResponseMessage Post([FromBody] Employee employee)
        {
            try { 
            using (EmployeeDBEntities entity = new EmployeeDBEntities())
            {

                entity.Employees.Add(employee);
                entity.SaveChanges();
                var message = Request.CreateResponse(HttpStatusCode.Created, employee);
                message.Headers.Location = new Uri(Request.RequestUri + employee.ID.ToString());
                return message;
            }
        }
            catch (Exception ex)
            {
              return  Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
                //var entity = entities.Employees.FirstDefault(n=>n.ID==I
                //entity.Employees.Add(employee);
                //entity.SaveChanges();
            
        }

    }
}
