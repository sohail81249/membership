﻿using System.Net.Http.Headers;

namespace EmployeeService
{
    internal class mediaTypeValue : MediaTypeHeaderValue
    {
        public mediaTypeValue(string mediaType) : base(mediaType)
        {
        }
    }
}