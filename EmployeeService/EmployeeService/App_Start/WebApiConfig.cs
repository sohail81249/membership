﻿using Microsoft.AspNetCore.Cors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;

namespace EmployeeService
{
    public static class WebApiConfig
    {

        public class CustomJsonFormater:JsonMediaTypeFormatter
        {
            //public CustomJsonFormater()
            //{
            //    this.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            //}

            public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
            {
                base.SetDefaultContentHeaders(type, headers, mediaType);
                headers.ContentType = new MediaTypeHeaderValue("application/json");
            }
        }
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes 
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
             );
            //config.Formatters.Add(new CustomJsonFormater ());
            EnableCorsAttribute cors = new EnableCorsAttribute("*");
            //config.EnableCors();
            config.Filters.Add(new RequireHttpAttribute());

        }
     }
}
